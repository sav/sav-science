The SAV-Science Package
-----------------------

A package that provides some commands that aim to help in generating
some of the most commonly used definitions in paper writing.

Please see the [manual](doc/manual.pdf) for instructions on how
to use the package.

Author: Michalis Kokologiannakis (michalis@mpi-sws.org)
